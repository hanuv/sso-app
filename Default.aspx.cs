﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;
using System.Windows.Forms;
using SSOWebApplicationArgenta.HawanedoWebReference;


namespace ArgentaSSOWebApp
{
    public partial class Default : System.Web.UI.Page
    {
        #region Declarations
        public string XMLReponseString { get; set; }
        public string SSOResponseString { get; set; }
        public string TokenType { get; set; }
        public string Token { get; set; }
        public string ThirdPartyID { get; set; }
        public string Ticket { get; set; }
        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.ToString().Contains("ClientFileID") != false && Request.QueryString.ToString().Contains("StudioSite") != false
                && Request.QueryString.ToString().Contains("StudioPage") != false)
            {
                string StartSessionDestinationURL = ConfigurationManager.AppSettings["StartSessionDestinationURL"];
                string StartSessionXML = BuildStartSessionXML().ToString();
                postStartSessionXMLData(StartSessionDestinationURL, StartSessionXML);


                string getFigloSSOSettings = ConfigurationManager.AppSettings["FigloSSOSettingsDestinationURL"];
                string SSOSettingsXML = BuildFigloSSOSettingsXML().ToString();
                postSSOData(getFigloSSOSettings, SSOSettingsXML);

                buildSSOURL();
            }          
        }
        #endregion

        #region BuildStartSessionXML
        public string BuildStartSessionXML()
        {
            StringBuilder SS = new StringBuilder();
            string userName = ConfigurationManager.AppSettings["StartSessionUsername"];
            string Password = ConfigurationManager.AppSettings["StartSessionPassword"];
            string requesStartSessionXML = "";

                SS.Append(@"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">");
                SS.Append("<soap:Body>");
                SS.Append(@"<StartSession xmlns=""http://schemas.figlo.com/Hawanedo/Business/v2.0c"">");
                SS.Append("<userName>" + userName + "</userName><password>" + Password + "</password></StartSession></soap:Body></soap:Envelope>");
                requesStartSessionXML = SS.ToString();

            

            return requesStartSessionXML;
        }
        #endregion

        #region BuildFigloSSOSettingsXML
        public string BuildFigloSSOSettingsXML()
        {
            string requestSSO = "";

                StringBuilder SSO = new StringBuilder();
                SSO.Append(@"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">");
                SSO.Append("<soap:Body>");
                SSO.Append(@"<GetFigloStudioSSOSettings xmlns=""http://schemas.figlo.com/Hawanedo/Business/v2.0c"">");
                SSO.Append("<securityToken>");
                SSO.Append(TokenType);
                SSO.Append(Token);
                SSO.Append("</securityToken>");
                SSO.Append("</GetFigloStudioSSOSettings>");
                SSO.Append("</soap:Body>");
                SSO.Append("</soap:Envelope>");
                requestSSO = SSO.ToString();



            return requestSSO;
        }
        #endregion

        #region AcceptAllCerts
        public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        #endregion

        #region SetupWebResponse
        public HttpWebResponse SetupWebResponse(string destinationURL, string XML)
        {
            HttpWebResponse response;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationURL);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(XML);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
            Stream requestStream = request.GetRequestStream();

            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            response = (HttpWebResponse)request.GetResponse();

            return response;
        }
        #endregion

        #region PostStartSessionXML
        public void postStartSessionXMLData(string destinationUrl, string requestXml)
        {
            int TokenTypeStartIndex = 0;
            int TokenTypeEndIndex = 0;
            int TokenStartIndex = 0;
            int TokenEndIndex = 0;

            HttpWebResponse response = SetupWebResponse(destinationUrl, requestXml);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                XMLReponseString = responseStr;

                if (XMLReponseString != "" && XMLReponseString != null)
                {
                    if (XMLReponseString.Contains("<TokenType>") && XMLReponseString.Contains("</TokenType>") && XMLReponseString.Contains("<Token>") && XMLReponseString.Contains("</Token>"))
                    {
                        TokenTypeStartIndex = XMLReponseString.IndexOf("<TokenType>");
                        TokenTypeEndIndex = XMLReponseString.IndexOf("</TokenType>") + "<TokenType>".Length;
                        TokenStartIndex = XMLReponseString.IndexOf("<Token>");
                        TokenEndIndex = XMLReponseString.IndexOf("</Token>") + "<Token>".Length;

                        TokenType = XMLReponseString.Substring(TokenTypeStartIndex, (TokenTypeEndIndex + 1) - TokenTypeStartIndex);
                        Token = XMLReponseString.Substring(TokenStartIndex, (TokenEndIndex + 1) - TokenStartIndex);
                    }
                }                       
            }
        }
        #endregion

        #region PostSSOData
        public void postSSOData(string destinationUrl, string requestSSO)
        {
            int TicketStartIndex = 0;
            int TicketEndIndex = 0;
            int ThirdPartyIDStartIndex = 0;
            int ThirdPartyIDEndIndex = 0;

            HttpWebResponse response = SetupWebResponse(destinationUrl, requestSSO);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                SSOResponseString = responseStr;

                if(SSOResponseString != "" && SSOResponseString != null)
                {
                    if (SSOResponseString.Contains("<Ticket>") && SSOResponseString.Contains("</Ticket>") && SSOResponseString.Contains("<ThirdPartyId>") && SSOResponseString.Contains("</ThirdPartyId>"))
                    {
                        TicketStartIndex = SSOResponseString.IndexOf("<Ticket>") + "<Ticket>".Length;
                        TicketEndIndex = SSOResponseString.IndexOf("</Ticket>");
                        ThirdPartyIDStartIndex = SSOResponseString.IndexOf("<ThirdPartyId>") + "<ThirdPartyId>".Length;
                        ThirdPartyIDEndIndex = SSOResponseString.IndexOf("</ThirdPartyId>");

                        ThirdPartyID = SSOResponseString.Substring(ThirdPartyIDStartIndex, (ThirdPartyIDEndIndex + 1) - ThirdPartyIDStartIndex - 1);
                        Ticket = SSOResponseString.Substring(TicketStartIndex, (TicketEndIndex + 1) - TicketStartIndex - 1);
                    }
                }                       
            }           
        }
        #endregion

        #region FetchClientAdvices
        public string FetchClientAdvice(string ClientFileID)
        {
            string AdviceId = "";
            string newToken = "";
            string newTokenType = "";
            //StringBuilder AdviceXML = new StringBuilder();
            //string FetchClientURL = ConfigurationManager.AppSettings["FetchClientFileDestinationURL"];

            //AdviceXML.Append(@"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">");
            //AdviceXML.Append("<soap:Body>");
            //AdviceXML.Append(@"<FetchClientFile xmlns=""http://schemas.figlo.com/Hawanedo/Business/v2.0c@"">");
            //AdviceXML.Append("<securityToken>");
            //AdviceXML.Append(TokenType);
            //AdviceXML.Append(Token);
            //AdviceXML.Append("</securityToken>");
            //AdviceXML.Append("<clientFileId>" + ClientFileID + "</clientFileId>");
            //AdviceXML.Append("</FetchClientFile></soap:Body></soap:Envelope>");            


            if (Token != "" && Token != null)
            {
                newToken = Token.Replace("<Token>", "");
                newToken = newToken.Replace("</Token>", "");
                newTokenType = TokenType.Replace("<TokenType>", "");
                newTokenType = newTokenType.Replace("</TokenType>", "");
            }
           

            Guid ClientID = new Guid(ClientFileID);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(newToken);
            HawanedoService serv = new HawanedoService();

            SecurityTokenHeader sth = new SecurityTokenHeader();
            sth.Token = doc.DocumentElement;
            sth.TokenType = newTokenType;

            SSOWebApplicationArgenta.HawanedoWebReference.Advice[] advice = new Advice[10];
            advice = serv.FetchClientFile(sth, ClientID).Advices;

            if(advice != null)
            {
                AdviceId = advice[0].AdviceID.ToString();
            }
           
            return AdviceId;
        }
        #endregion

        #region BuildSSOURL
        public string buildSSOURL()
        {
            string studioURL = ConfigurationManager.AppSettings["StudioURL"];
            string site = Request.QueryString["StudioSite"];
            string page = Request.QueryString["StudioPage"];
            string clientfileid = Request.QueryString["ClientFileID"];
            string thirdpartyid = ThirdPartyID;
            string ticket = Ticket;
            string baseurl = studioURL + "/" + site;
            string URLENcodedPageURL = System.Web.HttpUtility.UrlEncode(baseurl + "#!/" + page);
            string URLEncodedSSOPageURL = System.Web.HttpUtility.UrlEncode(baseurl);
            string RediURL = baseurl + "?retUrl=" + URLENcodedPageURL;
            string URLEncodedRedirURL = System.Web.HttpUtility.UrlEncode(RediURL);
            string EncodedURLTicket = System.Web.HttpUtility.UrlEncode(ticket);
            string AdviceID = FetchClientAdvice(clientfileid);

            string SSOUrl = "";
            string tURL = "";

            if ((clientfileid != "" && clientfileid != null) && (site != "" && site != null) && (page != "" && page != null))
            {
                tURL = studioURL + "/" + site + "/SSO?tpid=" + thirdpartyid + "&enticket=" + EncodedURLTicket + "&retUrl=" + URLEncodedSSOPageURL;

                //SSOUrl = tURL + "/Admin/SelectClient/" + clientfileid + "?adviceId=0" + "&retUrl=" + URLENcodedPageURL;

                SSOUrl = tURL + "/Admin/SelectClient/" + clientfileid + "?adviceId=" + AdviceID + "&retUrl=" + URLENcodedPageURL;

                Response.Redirect(SSOUrl);
            }

            return SSOUrl;
        }
        #endregion                      
    }
}